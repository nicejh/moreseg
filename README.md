**MoreSEG(Secure Email Gateway) 문서 관리**

[(주)소프트모아](http://www.softmore.kr)에서 출시한 통합메일보안 제품인 MoreSEG 의 WebAPI 등 외부연동 관련 문서 관리 사이트

---

## MoreSEG 란?

문서DRM 전문회사 [소프트캠프(주)](http://www.softcamp.co.kr)에서 출시한 Shieldex SaniTrans Mail 을 기반으로
가트너에서 제안한 SEG(Secure Email Gateway) 기능과 최신 보안기술을 적용하여 안전하게 메일을 열람할 수 있는 환경을 제공하는 제품
